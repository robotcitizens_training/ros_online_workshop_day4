#!/usr/bin/env python3
import rospy
from std_msgs.msg import String
from std_srvs.srv import Empty, EmptyResponse

class EnablePublish(object):
    def __init__(self):
        self.enable = False

        rospy.init_node("enable_pub",anonymous=True)

        self.pub = rospy.Publisher("/hello",String,queue_size=10)
        self.rate = rospy.Rate(10)

        rospy.Service("/enable_hello",Empty,self.callback_enable)
        rospy.Service("/disable_hello",Empty,self.callback_disable)

    def callback_enable(self,data):
        self.enable = True
        return EmptyResponse()

    def callback_disable(self,data):
        self.enable = False
        return EmptyResponse()

    def main(self):
        print("Waiting for service!!")
        while not rospy.is_shutdown():
            if self.enable:
                self.pub.publish("Hello")
            self.rate.sleep()

if __name__ == "__main__":
    enable_file = EnablePublish()
    enable_file.main()