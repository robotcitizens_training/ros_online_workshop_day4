#!/usr/bin/env python3
import rospy

from dynamic_reconfigure.server import Server
from my_package.cfg import MyPackageConfig

def callback(config, level):
    rospy.loginfo("""Reconfigure Request: {int_param}, {double_param},\ 
          {str_param}, {bool_param}, {size}""".format(**config))
    return config

if __name__ == "__main__":
    rospy.init_node("my_package", anonymous = False)

    srv = Server(MyPackageConfig, callback)
    rospy.spin()