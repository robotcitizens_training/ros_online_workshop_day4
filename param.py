#!/usr/bin/env python3
import rospy

rospy.set_param("test",2) #Set parameter "test" to 2

test = rospy.get_param("test") #Get value of parameter name "test"
print("Parameter 'test':",test)

print(rospy.has_param("test")) #Check parameter name "test" is available?
rospy.delete_param("test") #Delete parameter name "test"
print(rospy.has_param("test")) #Check parameter name "test" is available?